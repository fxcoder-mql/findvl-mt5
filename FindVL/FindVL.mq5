/*
Copyright 2023 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright   "FindVL 7.2. © FXcoder"
#property link        "https://fxcoder.blogspot.com"
#property description "FindVL: Find Volume Levels"
#property script_show_inputs

#include "FindVL-include/chart.mqh"
#include "FindVL-include/color.mqh"
#include "FindVL-include/go.mqh"
#include "FindVL-include/math.mqh"
#include "FindVL-include/mql.mqh"
#include "FindVL-include/series.mqh"

#include "FindVL-include/enum/price_step.mqh"
#include "FindVL-include/enum/ticks_type.mqh"
#include "FindVL-include/vp/vp_calc.mqh"
#include "FindVL-include/vp/vp_tick_params.mqh"


enum ENUM_FVL_PARAM
{
	FVL_PARAM_MAX     = 0x100,  // Maximum
	FVL_PARAM_MEDIAN  = 0x200,  // Median
	FVL_PARAM_VWAP    = 0x300,  // VWAP
};


input bool                   ClearOnly      = false;                 // Clear Only

input group "RANGE"
input int                    RangeBars      = 24;                    // Range Bars
input int                    Step           = 1;                     // Step

input group "DATA"
input ENUM_VP_SOURCE         DataSource     = VP_SOURCE_M1;          // Data Source
input ENUM_VP_BAR_TICKS      BarTicks       = VP_BAR_TICKS_OHLC;     // Bar Distribution

input ENUM_APPLIED_VOLUME    VolumeType     = VOLUME_TICK;           // Volume Type

input group "TICK"
input ENUM_TICKS_TYPE        TicksType      = TICKS_TYPE_ALL;        // Type of Requested Ticks
input ENUM_VP_TICK_PRICE     TickPriceType  = VP_TICK_PRICE_LAST_OR_AVG; // Price Type
input bool                   TickBid        = true;                  // Bid Price Changed
input bool                   TickAsk        = true;                  // Ask Price Changed
input bool                   TickLast       = true;                  // Last Price Changed
input bool                   TickVolume     = true;                  // Volume Changed
input bool                   TickBuy        = true;                  // Buy Deal
input bool                   TickSell       = true;                  // Sell Deal

input group "CALCULATION"
input ENUM_FVL_PARAM         Parameter      = FVL_PARAM_MAX;         // Parameter
input int                    Smooth         = 0;                     // Smooth Depth (0 => disable)
input ENUM_PRICE_STEP        PointScale     = PRICE_STEP_1;          // Price Step

input group "COLORS"
input color                  RangeColor     = clrRoyalBlue;          // Range Color
input color                  LevelColor     = clrLimeGreen;          // Level Color
input color                  NewLevelColor  = clrOrange;             // New Level Color

input group "SERVICE"
input string                 Id             = "fvl1";                // Id


void OnStart()
{
	string prefix = Id + " ";
	_chart.objects_delete_all(prefix);

	if (ClearOnly)
		return;

	bool show_range = _color.is_valid(RangeColor);
	bool show_level = _color.is_valid(LevelColor);
	bool show_new_level = _color.is_valid(NewLevelColor);

	if (!(show_range || show_level || show_new_level))
		return;

	CVPTickParams tick_params(TicksType, TickPriceType, TickBid, TickAsk, TickLast, TickVolume, TickBid, TickSell);

	double hg_point = _Point * PointScale;
	CVPDataParams data_params(DataSource, BarTicks, VolumeType);
	CVPCalc vpcalc_(data_params, hg_point, tick_params, QUANTILE_NONE);

	// forward range to determine the need to find the intersection
	MqlRates rate;
	if (!_series.rate(0, rate))
	{
		Print("Error: no zero bar.");
		return;
	}

	double low = rate.low;
	double high = rate.high;

	datetime horizon = vpcalc_.get_horizon();
	MqlRates rate_from, rate_to;
	double prev_level = 0;
	int prev_level_index = -1;
	int repeat_count = 1;

	for (int i = 0, nbars = _series.max_bars(); i < nbars; i += Step)
	{
		if (IsStopped())
			break;

		// get range

		int i_from = i + RangeBars - 1;

		if (!_series.rate(i_from, rate_from))
			break;

		if (!_series.rate(i, rate_to))
			break;

		// range
		datetime time_from = rate_from.time;
		datetime time_to   = rate_to.time;

		if (time_from < horizon)
			break;

		// update forward range
		bool res = true;
		for (int j = _math.max(i - Step, 0); j < i; j++)
		{
			if (!_series.rate(j, rate))
			{
				res = false;
				break;
			}

			low  = fmin(low,  rate.low);
			high = fmax(high, rate.high);
		}

		if (!res)
		{
			Print("Error while forward range updating.");
			break;
		}


		// calc hg and max

		double low_price;
		double volumes[];

		const int count = (DataSource == VP_SOURCE_TICKS)
			? vpcalc_.get_hg_by_ticks   (time_from, time_to, low_price, volumes)
			: vpcalc_.get_hg            (time_from, time_to, low_price, volumes);

		if (count <= 0)
		{
			break;
		}

		vpcalc_.smooth_hg(Smooth, hg_point, low_price, volumes);

		int level_pos = -1;//

		switch (Parameter)
		{
			case FVL_PARAM_MAX:
				level_pos = ArrayMaximum(volumes);
				break;

			case FVL_PARAM_MEDIAN:
				level_pos = _math.round_to_int((vpcalc_.quantile_pos(volumes, 0.5)));
				break;

			case FVL_PARAM_VWAP:
				level_pos = vpcalc_.hg_vwap_index(volumes, low_price, hg_point);
				break;

			default:
				Print("Error. Unknown Parameter.");
				break;
		}

		// draw

		double level = low_price + level_pos * hg_point;
		bool same_level = fabs(level - prev_level) < hg_point;
		repeat_count = same_level ? (repeat_count + 1) : 1;
		prev_level = level;

		int line_width = _math.max(_math.round_to_int(sqrt(repeat_count)), 1);

		const int level_index = /*same_level ? prev_level_index :*/ i;
		string indexed_prefix = prefix + (string)level_index;

		if (!same_level)
			prev_level_index = i;

		// draw range
		if (show_range)
			draw_trend(indexed_prefix + " rng", time_from, level, time_to, level, RangeColor, line_width, STYLE_SOLID, true,  false);

		if (level > high || level < low)
		{
			// naked/new forward
			if (show_new_level)
				draw_trend(indexed_prefix + " nfwd", time_to, level, time_to + _tf.current_seconds, level, NewLevelColor, line_width, STYLE_SOLID, false, true);
		}
		else
		{
			if (show_level)
			{
				// find crossing
				for (int j = i; j >= 0; j--)
				{
					if (!_series.rate(j, rate))
						break;

					if (level <= rate.high && level >= rate.low)
					{
						// forward
						datetime time3;
						// add a bar for good looking on lower tf
						if (!_time.add_periods(rate.time, 1, _tf.current, time3))
						{
							break;
						}

						draw_trend(indexed_prefix + " fwd", time_to, level, time3, level, LevelColor, line_width, STYLE_SOLID, false, false);
						break;
					}
				}
			}
		}

		if (((i + 1) / Step) % 73 == 0)
			Comment(_mql.program_name() + ": " + (string)(i + 1));
	}

	Comment("");
}

void draw_trend(string name, datetime time1, double price1, datetime time2, double price2,
	color lineColor = Gray, int width = 1, ENUM_LINE_STYLE style = STYLE_SOLID, bool back = true, bool ray = true, int window = 0)
{
	CGO trend(name);
	trend.redraw(OBJ_TREND, window, time1, price1, time2, price2);
	trend.fg_color(lineColor).style(style).width(width);
	trend.ray_left(false).ray_right(ray);
	trend.back(back).selectable(true).hidden(true);
}
