/*
Copyright 2023 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// Type of the requested ticks, see CopyTicks (flags). © FXcoder

enum ENUM_TICKS_TYPE
{
	TICKS_TYPE_ALL   = 0x100, // All ticks
	TICKS_TYPE_TRADE = 0x200, // Ticks with changes in Last and Volume
	TICKS_TYPE_INFO  = 0x300, // Ticks with Bid and/or Ask changes
};

uint EnumTicksTypeToCopyRatesFlags(ENUM_TICKS_TYPE ticks_type)
{
	if (ticks_type == TICKS_TYPE_ALL)
		return COPY_TICKS_ALL;

	if (ticks_type == TICKS_TYPE_TRADE)
		return COPY_TICKS_TRADE;

	if (ticks_type == TICKS_TYPE_INFO)
		return COPY_TICKS_INFO;

	return 0;
}
