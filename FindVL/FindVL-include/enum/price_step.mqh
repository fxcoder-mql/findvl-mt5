/*
Copyright 2023 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// Price step. © FXcoder

enum ENUM_PRICE_STEP
{
	PRICE_STEP_1     = 1,    // 1 Point
	PRICE_STEP_5     = 5,    // 5 Points
	PRICE_STEP_10    = 10,   // 10 Points
	PRICE_STEP_50    = 50,   // 50 Points
	PRICE_STEP_100   = 100,  // 100 Points
};
