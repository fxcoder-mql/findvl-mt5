/*
Copyright 2023 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// VP data parameters. © FXcoder

#include "enum/vp_bar_ticks.mqh"

#include "enum/vp_source.mqh"
#include "../tf.mqh"

class CVPDataParams
{
public:

	const ENUM_VP_SOURCE         source;
	const ENUM_VP_BAR_TICKS      bar_ticks;
	const ENUM_APPLIED_VOLUME    volume_type;
	const ENUM_TIMEFRAMES        tf;


public:

	void CVPDataParams(
		ENUM_VP_SOURCE         source_,
		ENUM_VP_BAR_TICKS      bar_ticks_,
		ENUM_APPLIED_VOLUME    volume_type_
	):
		source(source_),
		bar_ticks(bar_ticks_),
		volume_type(volume_type_),
		tf(_tf.find_closest(EnumVPSourceToMinutes(source_)))
	{
	}

	// copy constructor
	void CVPDataParams(
		const CVPDataParams &p
	):
		source(p.source),
		bar_ticks(p.bar_ticks),
		volume_type(p.volume_type),
		tf(p.tf)
	{
	}

};
